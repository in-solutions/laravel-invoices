<?php

namespace Insolutions\Invoices\Services;

use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Insolutions\Invoices\Models\Invoice;
use Insolutions\Invoices\Models\InvoiceItem;
use Insolutions\Invoices\Models\InvoiceType;
use Insolutions\Invoices\Models\InvoiceQueue;

use Insolutions\Invoices\Models\Customer;

class InvoiceService
{

	/**
	 * @param $data - array that contains all data needed for creating invoice
	 * @param array $invoiceItemsData
	 * @return Invoice
	 */
	public function create($data, Customer $customer) {
		$data["type_id"] = InvoiceType::PROFORMA;
		$invoice = new Invoice();
		$invoice->customer()->associate($customer);
		$invoice->fill($data);
		$invoice->save();

		return $invoice;
	}

	/**
	 * Deletes all invoice items of given invoice and stores new instead
	 * @param Invoice $invoice
	 * @param array $invoiceItemsData
	 * @return array
	 */
	public function saveInvoiceItems(Invoice $invoice, $invoiceItemsData = []) {
		$currentInvoiceItems = $invoice->invoiceItems;
		$final = [];
		// keep old invoice items and update them with new data, remove invoice items not present in given invoice items data
		foreach($currentInvoiceItems as $currentInvoiceItem) {
			$found = false;
			foreach($invoiceItemsData as $invoiceItemData) {
				if(isset($invoiceItemData["id"]) && $invoiceItemData["id"] == $currentInvoiceItem->id) {
					$currentInvoiceItem->fill($invoiceItemData);
					$currentInvoiceItem->invoice_id = $invoice->id;
					$currentInvoiceItem->save();
					$final[] = $currentInvoiceItem;
					$found = true;
					break;
				}
			}
			if(!$found) {
				$currentInvoiceItem->delete();
			}
		}

		foreach($invoiceItemsData as $invoiceItemData) {
			if(!isset($invoiceItemData["id"])) {
				$invoiceItemData["invoice_id"] = $invoice->id;
				$final[] = InvoiceItem::create($invoiceItemData);
			}
		}

		return $final;

	}

	/**
	 * @param Invoice $invoice
	 * @param $data - array that contains all data needed for updating invoice
	 * @return Invoice
	 */
	public function update(Invoice $invoice, $data, Customer $customer)
	{
		$invoice->fill($data);
		$invoice->customer()->associate($customer);
		return $invoice;
	}

	public function issue(Invoice $invoice)
	{
		DB::beginTransaction();

		if($invoice->type->id != InvoiceType::PROFORMA) {
			return false;
		}
		$invoice->queue->last_number = $invoice->queue->last_number + 1;
		$invoice->number = date("y") .  str_pad($invoice->queue->last_number, 6, "0", STR_PAD_LEFT);
		$invoice->type_id = InvoiceType::TAX_DOCUMENT;
		$invoice->save();
		$invoice->queue->save();

		DB::commit();


		return $invoice;

	}

	/**
	 * @param Invoice $invoice
	 * @return bool|Invoice
	 */
	public function postPayment(Invoice $invoice)
	{
		if(!$invoice->isPaid()) {
			return false;
		}

		return $this->issue($invoice);
	}

	/**
	 * @param Invoice $invoice
	 * @return Pdf
	 */
	public function generatePdf(Invoice $invoice) {
		$invoice->loadAllRelations();

		$data = [
			"invoice" => $invoice
		];
		$pdf = PDF::loadView('insolutions/invoices/invoice_template', $data);
		return $pdf;
	}

}