<?php

namespace Insolutions\Invoices\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Insolutions\I18n\Facades\Curr;

use Insolutions\Invoices\Models\Product;

/**
 * Insolutions\Invoices\Models\Invoice
 *
 * @property int $id
 * @property string $name
 * @property string $invoice_id
 * @property float $price_wo_vat
 * @property float $price_w_vat
 * @property int $currency_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static Invoice whereInvoiceId($value)
 * @method static Invoice whereCreatedAt($value)
 * @method static Invoice whereId($value)
 * @method static Invoice whereNumber($value)
 * @method static Invoice whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class InvoiceItem extends Model
{
	const VALIDATOR_DATA = [
		//"invoice_id"	=> "integer",
		"text"	        => "string|required",
		"price_w_vat"	=> "numeric|required",
		"price_wo_vat"	=> "numeric|required",
		"vat_rate"		=> "numeric|required",
		"amount"		=> "numeric|required",
	];


	use SoftDeletes;

	protected $table = 't_invoice_item';
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];
	protected $fillable = ['invoice_id', 'text', 'amount', 'vat_rate', 'price_wo_vat', 'price_w_vat'];
	protected $hidden = ['invoice_id', 'deleted_at','product_id'];

	public $with = ['product'];

	public function invoice(): BelongsTo
	{
		return $this->belongsTo(InvoiceQueue::class);
	}

	public static function loadRelations(Invoice $order): Invoice
	{
		return $order->load([
			'invoice',
		]);
	}

	public function product() {
		return $this->belongsTo(Product::class);
	}
}