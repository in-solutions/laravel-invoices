<?php

namespace Insolutions\Invoices\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Insolutions\I18n\Facades\Curr;

use Insolutions\Invoices\Models\Customer;
use Insolutions\Invoices\Models\PaymentType;

use Insolutions\I18n\Currency;

use Carbon\Carbon;

use Dimsav\Translatable\Translatable;

/**
 * Insolutions\Invoices\Invoice
 *
 * @property int $id
 * @property string $name
 * @property string $queue_id
 * @property string $number
 * @property string $type_id
 * @property string $client_id
 * @property string $date_issue
 * @property string $date_due
 * @property string $date_tax
 * @property string $sum_wo_vat
 * @property string $sum_w_vat
 * @property int $currency_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static Invoice whereQueueId($value)
 * @method static Invoice whereCreatedAt($value)
 * @method static Invoice whereId($value)
 * @method static Invoice whereNumber($value)
 * @method static Invoice whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Invoice extends Model
{
	const VALIDATOR_DATA = [
		//"queue_id"	=> "integer|required",
		//"client_id"	=> "integer|required",
		"date_issue"	=> "string|required",
		"date_due"	=> "string|required",
		"date_tax"	=> "string|required",
	];

	use SoftDeletes, Translatable;

    protected $table = 't_invoice';
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];
	protected $fillable = ['queue_id', 'type_id', 'date_issue', 'date_due', 'date_tax', 'sum_wo_vat', 'sum_w_vat', 'currency_code','vs','cs','ss','order_id','vat_applicable'];
	protected $hidden = ['queue_id','deleted_at','type_id','customer_id','payment_type_id'];

	public $translatedAttributes = ['note'];

	protected $casts = [
        'vat_applicable' => 'boolean',
    ];

	public $timestamps = [
		'created_at',
		'updated_at',
		'deleted_at',
		'paid_at'
	];

	protected static function boot()
	{
		parent::boot();

		Invoice::saving(function (Invoice $invoice) {
			if(!$invoice->currency_code) {
				$invoice->currency_code = Curr::get()->code;
			}
		});

	}

	public function items(): HasMany
	{
		return $this->hasMany(InvoiceItem::class);
	}

	public function queue(): BelongsTo
	{
		return $this->belongsTo(InvoiceQueue::class, 'queue_id', 'id');
	}

	public function type(): BelongsTo
	{
		return $this->belongsTo(InvoiceType::class);
	}

	public function currency(): BelongsTo
	{
		return $this->belongsTo(Currency::class, 'currency_code', 'code');
	}

	public function customer(): BelongsTo
	{
		return $this->belongsTo(Customer::class);
	}

	public function paymentType(): BelongsTo
	{
		return $this->belongsTo(PaymentType::class);
	}

	public function loadRelations(Invoice $invoice): Invoice
	{
		return $invoice->loadAllRelations();
	}

	public function loadAllRelations(): Invoice {
		return $this->load([
			// 'client.invoiceAddress',
			'type',
			'queue',
			'items',
			'currency',
			'customer.person',
			'customer.company',
			'customer.invoiceAddress.country',
            'paymentType',
		]);
	}

	public function setPaid(bool $val) {
		$this->paid_at = ($val ? Carbon::now() : null);
		return $this->save();
	}

	/**
	 * Checks if invoice is paid
	 * @return bool
	 */
	public function isPaid() {
		return !is_null($this->paid_at);
	}

	public function recalculateTotalPrice()
	{
		self::recalculateTotalPriceForItems($this, $this->items);

	}


	/**
	 * @param $invoice $invoice
	 * @param InvoiceItem[] $invoiceItems
	 */
	public static function recalculateTotalPriceForItems($invoice, $invoiceItems)
	{
		$totalPriceWVat = 0;
		$totalPriceWoVat = 0;

		foreach($invoiceItems as $invoiceItem) {
			$totalPriceWVat += $invoiceItem->price_w_vat * $invoiceItem->amount;
			$totalPriceWoVat += $invoiceItem->price_wo_vat * $invoiceItem->amount;
		}
		$invoice->sum_w_vat = $totalPriceWVat;
		$invoice->sum_wo_vat = $totalPriceWoVat;

		return $invoice;
	}
}
