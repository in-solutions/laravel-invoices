<?php

namespace Insolutions\Invoices\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Dimsav\Translatable\Translatable;

use Insolutions\Invoices\Models\Company;
/**
 * Insolutions\Invoices\Models\InvoiceQueue
 *
 * @property int $id
 * @property string $title
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static InvoiceQueue whereCreatedAt($value)
 * @method static InvoiceQueue whereId($value)
 * @method static InvoiceQueue whereName($value)
 * @method static InvoiceQueue whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class InvoiceQueue extends Model
{
	const VALIDATOR_DATA = [
		"title"	=> "string|required",
	];

	use SoftDeletes, Translatable;

    protected $table = 't_invoice_queue';
	protected $dates = ['deleted_at'];
	protected $fillable = ['title','last_number'];
	protected $hidden = ['deleted_at','company_id'];

	public $translatedAttributes = ['legal_text'];

	protected $with = ['company.address'];

	public static function getDefault() {
		return self::first();
	}

	public function company() {
		return $this->belongsTo(Company::class);
	}
}