<?php

namespace Insolutions\Invoices\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use Insolutions\I18n\Currency;

class ProductPrice extends Model
{
    use SoftDeletes;
	
	protected $table = 't_product_price';
	
	protected $fillable = [
		'product_id',
		'vat_rate',
		'currency_code',
		'unit_price_wo_vat',
		'unit_price_with_vat',
	];

	protected $hidden = [
		'product_id',
        'created_at',
		'updated_at',
		'deleted_at'
    ];

	protected $guarded = [];

	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at'
	];
	
	public function product() {
		return $this->belongsTo(Product::class);
	}

	public function currency() {
		return $this->belongsTo(Currency::class, 'currency_code', 'code');
	}
}
