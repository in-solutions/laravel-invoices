<?php

namespace Insolutions\Invoices\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Insolutions\I18n\Country;

class Address extends Model
{
	use SoftDeletes;

    protected $table = 't_address';

    protected $fillable = ['street', 'city', 'zip'];

    protected $hidden = ['deleted_at','created_at','updated_at','country_id'];

    protected $with = ['country'];

    public function country() {
    	return $this->belongsTo(Country::class);
    }

}