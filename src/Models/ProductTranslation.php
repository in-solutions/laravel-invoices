<?php

namespace Insolutions\Invoices\Models;

use Illuminate\Database\Eloquent\Model;

class ProductTranslation extends Model
{    

	protected $table = 't_product_tsl';
	
	protected $fillable = [
		'title',
		'description',
	];

	protected $hidden = [
        'id',
        'product_id'
    ];

	public $timestamps = false;
	
	public function product() {
		return $this->belongsTo(Product::class);
	}
}
