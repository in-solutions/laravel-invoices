<?php

namespace Insolutions\Invoices\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Insolutions\Invoices\Models\InvoiceTypeTranslation
 *
 * @property int $id
 * @property int $order_state_id
 * @property string $language_code
 * @property string $title
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\Invoices\Models\InvoiceTypeTranslation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\Invoices\Models\InvoiceTypeTranslation whereLanguageCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\Invoices\Models\InvoiceTypeTranslation whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\Invoices\Models\InvoiceTypeTranslation whereInvoiceTypeId($value)
 * @mixin \Eloquent
 */
class InvoiceTypeTranslation extends Model
{
    protected $table = 'enm_invoice_type_translations';
    public $timestamps = false;
    protected $fillable = ['title'];
    protected $hidden = ['id', 'invoice_type_id'];
}