<?php

namespace Insolutions\Invoices\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Support\Facades\Log;

class Customer extends Model
{
    use SoftDeletes;

    protected $table = 't_customer';

    const TYPE_PERSON = 'person';
    const TYPE_COMPANY = 'company';

    protected $fillable = ['discount'];
    
    protected $hidden = ['deleted_at','created_at','updated_at', 'person_id', 'company_id', 'invoice_address_id'];

    protected $appends = ['display_name'];

    public function getDisplayNameAttribute() {
        return ($this->getType() == self::TYPE_COMPANY) ? $this->company->name : $this->person->full_name;
    }

    public function getTypeAttribute() {
        return $this->getType();
    }

    public function getType() {
    	if ($this->company) {
    		return self::TYPE_COMPANY;
    	} else if ($this->person) {
    		return self::TYPE_PERSON;
    	} else {
    		Log::error("Customer has indetermine type!", ['id' => $this->id]);
    		return null;
    	}
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function person()
    {
    	return $this->belongsTo(Person::class);
    }

    public function invoiceAddress() {
    	return $this->belongsTo(Address::class);
    }
}