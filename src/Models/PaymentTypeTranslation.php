<?php

namespace Insolutions\Invoices\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentTypeTranslation extends Model
{
    protected $table = 'enm_payment_type_tsl';
    public $timestamps = false;
    protected $fillable = ['title'];
    protected $hidden = ['id', 'payment_type_id'];
}