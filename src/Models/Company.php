<?php

namespace Insolutions\Invoices\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
	use SoftDeletes;

    protected $table = 't_company';

    protected $hidden = ['deleted_at','created_at','updated_at','address_id'];
    
    protected $fillable = ['name','reg_id', 'vat_id','tax_id', 'vat_applicable'];

    protected $casts = [
        'vat_applicable' => 'boolean',
    ];

    public function address() {
    	return $this->belongsTo(Address::class);
    }
}