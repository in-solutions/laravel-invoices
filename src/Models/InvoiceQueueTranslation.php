<?php

namespace Insolutions\Invoices\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Insolutions\Invoices\Models\InvoiceTypeTranslation
 *
 * @property int $id
 * @property int $order_state_id
 * @property string $language_code
 * @property string $title
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\Invoices\Models\InvoiceTypeTranslation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\Invoices\Models\InvoiceTypeTranslation whereLanguageCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\Invoices\Models\InvoiceTypeTranslation whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\Invoices\Models\InvoiceTypeTranslation whereInvoiceTypeId($value)
 * @mixin \Eloquent
 */
class InvoiceQueueTranslation extends Model
{
    protected $table = 't_invoice_queue_tsl';
    public $timestamps = false;
    protected $fillable = ['legal_text'];
    protected $hidden = ['id', 'invoice_queue_id'];
}