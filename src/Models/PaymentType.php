<?php

namespace Insolutions\Invoices\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class PaymentType extends Model
{
    use Translatable;

    protected $table = 'enm_payment_type';
    public $translatedAttributes = ['title'];
    public $timestamps = null;
}