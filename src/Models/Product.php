<?php

namespace Insolutions\Invoices\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes, Translatable;

	protected $table = 't_product';

	public $translatedAttributes = ['title', 'description'];

	protected $fillable = ['code'];

	protected $hidden = [
        'created_at',
		'updated_at',
		'deleted_at'
    ];

	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at'
	];

	public function prices() {
		return $this->hasMany(ProductPrice::class);
	}

}
