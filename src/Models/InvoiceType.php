<?php

namespace Insolutions\Invoices\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Insolutions\Invoices\Models\InvoiceType
 *
 * @property int $id
 * @property int $position
 * @property-read \Illuminate\Database\Eloquent\Collection|\Insolutions\Invoices\Models\InvoiceTypeTranslation[] $translations
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\Invoices\Models\InvoiceType listsTranslations($translationField)
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\Invoices\Models\InvoiceType notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\Invoices\Models\InvoiceType translated()
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\Invoices\Models\InvoiceType translatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\Invoices\Models\InvoiceType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\Invoices\Models\InvoiceType wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\Invoices\Models\InvoiceType whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\Invoices\Models\InvoiceType whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\Invoices\Models\InvoiceType withTranslation()
 * @mixin \Eloquent
 */
class InvoiceType extends Model
{
    use Translatable;

    const    PROFORMA = 1,
					TAX_DOCUMENT = 2
					;

    protected $table = 'enm_invoice_type';
    protected $hidden = ['position'];
    public $translatedAttributes = ['title'];

    public static function getAllSorted(): Collection
    {
        return self::orderBy('position')->get();
    }
}