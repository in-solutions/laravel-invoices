<?php

namespace Insolutions\Invoices\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Person extends Model
{
	use SoftDeletes;

    protected $table = 't_person';

    protected $hidden = ['deleted_at','created_at','updated_at'];

    protected $fillable = ['first_name', 'last_name'];

    public function getFullNameAttribute() {
    	return $this->first_name . ' ' . $this->last_name;
    }

}