<?php

namespace Insolutions\Invoices;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {        
        
        $this->publishes([
            __DIR__.'/../db' => base_path('database/sql/insolutions/invoices'),
//            __DIR__.'/../app/Listeners' => app_path('Listeners'),
            __DIR__.'/../resources/views' => base_path('resources/views/insolutions/invoices'),        
//            __DIR__.'/assets' => public_path('ins/ecommerce'),
        ]);

        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'invoices');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes.php';
        $this->app->make('Insolutions\Invoices\Controllers\InvoiceController');
        $this->app->make('Insolutions\Invoices\Controllers\InvoiceQueueController');
        $this->app->make('Insolutions\Invoices\Controllers\CustomerController');
    }
}
