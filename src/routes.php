<?php

use Insolutions\Invoices\Controllers\InvoiceQueueController;
use Insolutions\Invoices\Controllers\InvoiceController;
use Insolutions\Invoices\Controllers\CustomerController;
use Insolutions\Invoices\Controllers\ProductController;


Route::group(['prefix' => 'invoices'], function () {

	Route::get('types', InvoiceController::class . '@getInvoiceTypes');

	Route::apiResource('invoiceQueue', InvoiceQueueController::class);
	Route::apiResource('invoiceQueue/{invoice_queue_id}/invoice', InvoiceController::class, ['only' => ['index', 'store']]);
	Route::apiResource('invoice', InvoiceController::class, ['only' => ['show', 'update', 'destroy']]);

	Route::get('/invoice/{id}/item', InvoiceController::class . '@itemAutosuggest');

	Route::patch('/invoice/{id}/issue', InvoiceController::class ."@issue");
	Route::patch('/invoice/{id}/paid', InvoiceController::class ."@setPaid");
	Route::patch('/invoice/{id}/unpaid', InvoiceController::class ."@setUnpaid");
	
	Route::get('/invoice/{id}/view', InvoiceController::class ."@view");
	Route::get('/invoice/{id}/pdf', InvoiceController::class ."@generatePdf");

});

Route::group(['prefix' => 'payments'], function () {
	
	Route::get('types', InvoiceController::class . '@getPaymentTypes');
	
});

Route::group(['prefix' => 'products'], function () {
	
	Route::get('product', ProductController::class . '@getProducts');
	Route::get('product/{product_id}', ProductController::class . '@show');

	Route::post('product', ProductController::class . '@save');
	Route::put('product/{product_id}', ProductController::class . '@save');
	Route::delete('product/{product_id}', ProductController::class . '@destroy');

});

Route::group(['prefix' => 'common'], function () {
	
	Route::get('customer', CustomerController::class . '@getCustomers');
	Route::get('customer/{customer_id}', CustomerController::class . '@show');

	Route::post('customers', CustomerController::class . '@create');
	Route::put('customers/{customer_id}', CustomerController::class . '@update');
	Route::delete('customers/{customer_id}', CustomerController::class . '@destroy');

});