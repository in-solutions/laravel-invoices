<?php

namespace Insolutions\Invoices\Controllers;
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Insolutions\Auth\MiddlewareOnlyAuth;

use Insolutions\Invoices\Models\Product;
use Insolutions\Invoices\Models\ProductPrice;

class ProductController extends \App\Http\Controllers\Controller
{
	public function __construct()
	{
		// makes sure, only logged users will proceed in this controller
		$this->middleware(MiddlewareOnlyAuth::class);
	}

	public function destroy($product_id) {
		$product = Product::findOrFail($product_id);

		DB::beginTransaction();
		try {
			$product->translations()->delete();
			$product->prices()->delete();
			$product->delete();
		} catch (Exception $e) {
			DB::rollBack();
			throw $e;
		}
		DB::commit();


	}

	public function save(Request $r, $product_id = null) {
		$product = $product_id ? Product::findOrFail($product_id) : new Product;

		$product->fill($r->all());

		DB::beginTransaction();
		try {
			// prepare translation data to [en => [title => xxx], de => [title => yyy]]
	    	$tslData = [];
	    	foreach ($r->translations as $tsl) {
	    		$tslData[$tsl['language_code']] = $tsl;
	    	}
	    	$product->fill($tslData); // set translation data
	    	$product->save();

	    	$priceIDsToKeep = [];
	    	// === [prices saving] ===
	    	foreach ($r->prices as $priceData) {
	    		if (isset($priceData['id'])) {
	    			$price = $product->prices()->where(['id' => $priceData['id']])->first();
	    			if (!$price) {
	    				abort(404, "Price #{$priceData['id']} not found for product #{$product->id}");
	    			}
	    		} else {
	    			$price = new ProductPrice;
	    		}

	    		$price->fill($priceData);
	    		$price = $product->prices()->save($price);
	    		$priceIDsToKeep[] = $price->id;
	    	}

	    	$product->prices()->whereNotIn('id', $priceIDsToKeep)->delete();

	    	DB::commit();
	    } catch (Exception $e) {
	    	DB::rollBack();
	    }

	    $product->load('prices');

    	return response()->json($product);
	}

    public function show(Request $r, $product_id) {
    	return response()->json(
    		Product::with(['prices'])->findOrFail($product_id)
    	);
    }

	public function getProducts(Request $r) {
		$filterName = $r->q ?: null;

		$qb = Product::with(['prices']);
		
		if ($r->q) {
			$qb->whereTranslationLike("title", "%{$r->q}%");

			$qb->orWhere("code", 'LIKE', "%{$r->q}%");
		}

		$result = $qb->paginate($r->perPage ?: 50);

		return response()->json($result);
	}

}