<?php

namespace Insolutions\Invoices\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Insolutions\Invoices\Models\InvoiceQueue;
use Validator;

class InvoiceQueueController extends Controller
{

    public function index(): JsonResponse
    {
        $invoiceQueues = InvoiceQueue::paginate();

        return response()->json($invoiceQueues);
    }

    public function show(int $id): JsonResponse
    {
        $invoiceQueue = InvoiceQueue::findOrFail($id);

        return response()->json($invoiceQueue);
    }

    public function store(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), InvoiceQueue::VALIDATOR_DATA);

        if ($validator->fails()) {
            return response($validator->errors()->all(), 422); // unprocessable
        }

        $invoiceQueue = InvoiceQueue::create($request->only(['title']));
        $invoiceQueue->save();
        return response()->json($invoiceQueue);
    }

    public function update(Request $request, int $id): JsonResponse
    {
        $validator = Validator::make($request->all(), InvoiceQueue::VALIDATOR_DATA);

        if ($validator->fails()) {
            return response($validator->errors()->all(), 422); // unprocessable
        }

        $invoiceQueue = InvoiceQueue::findOrFail($id);

        $invoiceQueue->fill($request->only(['title', 'last_number']));
        $invoiceQueue->save();
        return response()->json($invoiceQueue);
    }

	public function destroy(int $id): JsonResponse
	{
		$invoiceQueue = InvoiceQueue::findOrFail($id);

		$invoiceQueue->delete();

		return response()->json($invoiceQueue);
	}


}