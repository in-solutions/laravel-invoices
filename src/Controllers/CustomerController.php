<?php

namespace Insolutions\Invoices\Controllers;
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Insolutions\Invoices\Services\CustomerService;

use Insolutions\Invoices\Models\Person;
use Insolutions\Invoices\Models\Company;
use Insolutions\Invoices\Models\Address;
use Insolutions\Invoices\Models\Customer;
use Insolutions\I18n\Country;

class CustomerController extends \App\Http\Controllers\Controller
{
	public function destroy($customer_id) {
		$customer = Customer::findOrFail($customer_id);

		DB::beginTransaction();
		try {		

			$customer->delete();

			$customer->person->delete();
			$customer->invoiceAddress->delete();
			if ($customer->company) {
				$customer->company->delete();
			}
		} catch (Exception $e) {
			DB::rollBack();
		}

		DB::commit();
	}

	public function update(Request $r, $customer_id) {
		$customer = Customer::findOrFail($customer_id);

		$customer->fill($r->all());

		$person = $customer->person;
		$company = $customer->company;
		$address = $customer->invoiceAddress;

		DB::beginTransaction();
		try {

			$person->fill($r->person)->save();

			// COMPANY data processing
			if ($company && $r->company) {
				// company exists, and given data -> update company
				$company->fill($r->company)->save();
			} else if ($company && !$r->company) {
				// company exists, but not given data -> dissacociate & delete company
				$customer->company()->dissociate();
				$customer->save();
				$company->delete();
			} else if (!$company && $r->company) {
				// company not exists, but given data -> create company
				$customer->company()->associate(Company::create($r->company));
			}

			// INVOICE ADDRESS data processing
			$country = Country::findOrFail($r->invoice_address['country']['id']);
			if ($address && $r->invoice_address) {
				// address exists, and given data -> update address
				$address->country()->associate($country);
				$address->fill($r->invoice_address)->save();
			} else if ($address && !$r->invoice_address) {
				// address exists, but not given data -> dissacociate & delete address
				$customer->invoiceAddress()->dissociate();
				$customer->save();
				$address->delete();
			} else if (!$address && $r->invoice_address) {
				// address not exists, but given data -> create address
				$address = new Address;
				$address->country()->associate($country);
				$address->fill($r->invoice_address)->save();
				$customer->invoiceAddress()->associate($address);
			}

			$customer->save();
		} catch (Exception $e) {
			DB::rollBack();
			throw $e;
		}

		DB::commit();

		return response()->json($customer);
	}

    public function create(Request $r) {
    	
    	$customer = CustomerService::createCustomer($r->all());

	    return response()->json($customer);
    }

    public function show(Request $r, $customer_id) {
    	return response()->json(
    		Customer::with(['person', 'company', 'invoiceAddress'])->findOrFail($customer_id)
    	);
    }

	public function getCustomers(Request $r) {
		$filterName = $r->q ?: null;

		$qb = Customer::with(['person','company','invoiceAddress']);

		if ($filterName) {
			$qb->whereHas("company", function ($query) use ($filterName) {
				$query->whereNotNull('id');
				$query->where('name', 'LIKE', "%{$filterName}%");
			});
			$qb->orWhereHas("person", function ($query) use ($filterName) {
				$query->whereNotNull('id');
				$words = explode(' ', $filterName);
				foreach ($words as $word) {
					$query->where(\DB::raw('first_name'), 'LIKE', "%{$word}%");
					$query->orWhere(\DB::raw('last_name'), 'LIKE', "%{$word}%");
				}
			});
		}

		$result = $qb->paginate($r->perPage ?: 50);

		return response()->json($result);
	}

}