<?php

namespace Insolutions\Invoices\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

use Insolutions\Auth\MiddlewareOnlyAuth;

use Insolutions\Invoices\Models\Invoice;
use Insolutions\Invoices\Models\InvoiceType;
use Insolutions\Invoices\Models\InvoiceItem;
use Insolutions\Invoices\Models\InvoiceQueue;
use Insolutions\Invoices\Models\Customer;
use Insolutions\Invoices\Models\PaymentType;
use Insolutions\Invoices\Models\Product;

use Insolutions\Invoices\Services\InvoiceService;
use Insolutions\Invoices\Services\CustomerService;

use Validator;

class InvoiceController extends Controller
{

	private $invoiceService;

	public function __construct(InvoiceService $invoiceService)
	{
		$this->invoiceService = $invoiceService;

		// makes sure, only logged users will proceed in this controller
		$this->middleware(MiddlewareOnlyAuth::class)->except(['view','generatePdf']);
	}

	public function getPaymentTypes() {
		return response()->json(
			PaymentType::all()
		);
	}

	public function setPaid($invoice_id) {
		Invoice::findOrFail($invoice_id)->setPaid(true);
	}

	public function setUnpaid($invoice_id) {
		Invoice::findOrFail($invoice_id)->setPaid(false);
	}

	public function getInvoiceTypes(): JsonResponse {
		return response()->json(InvoiceType::all());
	}

	public function itemAutosuggest(Request $r): JsonResponse
	{
		$itemText = $r->q;

		$items = InvoiceItem::where('text', 'LIKE', "%{$itemText}%")
				->orderBy('updated_at', 'desc')
				->get()->makeHidden(['id','amount','created_at']);

		$outputItemsByText = [];
		$outputItems = [];

		foreach ($items as $item) {
			if (!isset($outputItems[$item->text])) {
				$outputItems[$item->text] = [
					'text' => $item->text,
					'prices' => []
				];
			}

			$outputItems[$item->text]['prices'][] = [
				'vat_rate' => $item->vat_rate,
				'price_wo_vat' => $item->price_wo_vat,
				'price_w_vat' => $item->price_w_vat,
			];

			$outputItems[$item->text]['prices'] = 
				array_map("unserialize", 
					array_unique(
						array_map("serialize", $outputItems[$item->text]['prices'])
					)
				);
		}

		return response()->json($outputItems);
	}

	public function index(Request $r, $invoice_queue_id): JsonResponse
	{
		$qb = Invoice::with(['type', 'currency','customer.person','customer.company'])->whereQueueId($invoice_queue_id);

		if ($r->type) {
			$typeIds = explode(',',trim($r->type));
			$types = [];
			foreach ($typeIds as $typeId) {
				InvoiceType::findOrFail($typeId); // check if exists
				$types[] = $typeId;
			}

			$qb->whereIn('type_id', $types);
		}
		
		if ($r->has('paid')) {
			if ($r->paid == 0) { 
				$qb->whereNull('paid_at');
			} else if ($r->paid == 1) {
				$qb->whereNotNull('paid_at');
			} else {
				abort(422, 'Value of ?paid= parameter not recognized');
			}
		}

		$invoices = $qb->orderBy('number', 'desc')->paginate($r->perPage ?: 50);

		return response()->json($invoices);
	}

	public function show(int $id): JsonResponse
	{
		$invoice = Invoice::findOrFail($id);

		$invoice->loadAllRelations();

		return response()->json($invoice);
	}

	public function store(Request $request, $invoice_queue_id): JsonResponse
	{
		$validator = Validator::make($request->all(), Invoice::VALIDATOR_DATA);

		if ($validator->fails()) {
			return response()->json($validator->errors()->all(), 422);
		}

		$data = $request->all();
		
		$data['queue_id'] = InvoiceQueue::findOrFail($invoice_queue_id)->id; // includes verification, that queue exists

		$itemsData = isset($data['items']) ? $data['items'] : [];
		foreach($itemsData as $index => $itemData) {
			$validator = Validator::make($itemData, InvoiceItem::VALIDATOR_DATA);
			if ($validator->fails()) {
				return response()->json(["items[$index]" => $validator->errors()->all()], 422);
			}
		}

		// validation ends
		$customer = null;
		if (isset($request->customer['id'])) {
			$customer = Customer::findOrFail($request->customer['id']);
		} else if ($request->customer != null) {
			$customer = CustomerService::createCustomer($request->customer);
		}

		if (!$customer) {
			return response("Invalid Customer. Must have ID attribute OR data for creating new", 422);
		}

		$invoice = $this->invoiceService->create(
			$data,
			$customer
		);

		if (isset($request->payment_type['id']) && ($request->payment_type['id'] != null)) {
			$invoice->paymentType()->associate(
				PaymentType::findOrFail($request->payment_type['id'])
			);
		}

		foreach ((array)$request->translations as $tsl) {
			$code = $tsl['language_code'];
			unset($tsl['language_code']);

			foreach ($tsl as $field => $value) {
				$invoice->{$field . ':' . $code} = $value;
			}
		}

		foreach ($itemsData as $itemData) {
			$item = new InvoiceItem($itemData);
			
			if (isset($itemData['product']['id'])) {				
				$item->product()->associate(
					Product::findOrFail($itemData['product']['id'])
				);
			}

			$invoice->items()->save($item);
		}
		//$invoiceItems = $this->invoiceService->saveInvoiceItems($invoice, $itemsData);
		$invoice->recalculateTotalPrice();
		$invoice->save();

		$invoice->loadAllRelations();

		return response()->json($invoice);
	}

	public function update(Request $request, int $id): JsonResponse
	{
		$validator = Validator::make($request->all(), Invoice::VALIDATOR_DATA);

		if ($validator->fails()) {
			return response()->json($validator->errors()->all(), 422);
		}

		$itemsData = $request->all()["items"];
		foreach($itemsData as $index => $itemData) {
			$validator = Validator::make($itemData, InvoiceItem::VALIDATOR_DATA);
			if ($validator->fails()) {
				return response()->json(["items[$index]" => $validator->errors()->all()], 422);
			}
		}

		$invoice = Invoice::findOrFail($id);

		$data = $request->all();
		unset($data["items"]);


		DB::beginTransaction();
		$invoice = $this->invoiceService->update($invoice, $data, Customer::findOrFail($request->customer['id']));

		if (isset($request->payment_type['id']) && ($request->payment_type['id'] != null)) {			
			$invoice->paymentType()->associate(
				PaymentType::findOrFail($request->payment_type['id'])
			);
		} else {
			$invoice->paymentType()->dissociate();
		}

		foreach ((array)$request->translations as $tsl) {
			$code = $tsl['language_code'];
			unset($tsl['language_code']);
			
			foreach ($tsl as $field => $value) {
				$invoice->{$field . ':' . $code} = $value;
			}
		}

		$itemIDsToKeep = [];
		foreach ($itemsData as $itemData) {
			if (isset($itemData['id'])) {
				$item = InvoiceItem::findOrFail($itemData['id']);
				$item->fill($itemData);				
				$item->save();
			} else {
				$item = $invoice->items()->save(new InvoiceItem($itemData));
			}

			if (isset($itemData['product']['id']) && $itemData['product']['id']) {
				$item->product()->associate(
					Product::findOrFail($itemData['product']['id'])
				);
			} else {
				$item->product()->dissociate();
			}
			$item->save();

			$itemIDsToKeep[] = $item->id;
		}

		$invoice->items()->whereNotIn('id', $itemIDsToKeep)->delete();
		//$invoiceItems = $this->invoiceService->saveInvoiceItems($invoice, $invoiceItemsData);
		$invoice->recalculateTotalPrice();

		$invoice->save();
		DB::commit();

		$invoice->loadAllRelations();

		return response()->json($invoice);
	}

	public function destroy(int $id): JsonResponse
	{
		$invoice = Invoice::findOrFail($id);

		$invoice->delete();

		return response()->json($invoice);
	}

	public function issue(int $id)
	{
		$invoice = Invoice::findOrFail($id);

		if ($invoice->type->id != InvoiceType::PROFORMA) {
			return response("Invoice is already issued" ,422);
		}

		$invoice = $this->invoiceService->issue($invoice);
		
		$invoice->loadAllRelations();

		return response()->json($invoice);
	}

	public function view(Request $r, $id) {
		if ($r->lang) {
			\App::setLocale($r->lang);
		}
		$invoice = Invoice::findOrFail($id);
		$invoice->loadAllRelations();
		return view('insolutions/invoices/invoice_template', ['invoice' => $invoice]);
	}

	public function generatePdf(Request $r, $id) {
		if ($r->lang) {
			\App::setLocale($r->lang);
		}

		$pdf = $this->invoiceService->generatePdf(Invoice::findOrFail($id));
		return $pdf->download();
	}



}
