# README #

This README of generic invoice backend package

## What is this repository for? ##

Package covers sample package functionality including database, routes etc.

## How do I get set up? ##

### 1. composer.json ###

`composer require insolutions/laravel-invoices`

### 2. Publish sources ###

`php artisan vendor:publish`

- publishes db migrations into /database/sql/ins/package
- publishes listeners/subscribers into /app/Listeners
- publisher view for invoice PDF into views/insolutions/invoices

### 3. Register package ServiceProvider ###

*not needed in Laravel >5.5*

in file _app/config.php_ extend array by line:

```
"providers" => [
	
	...
		
	Insolutions\Invoices\ServiceProvider::class,		
	
]
```

## Modules interface ##

Packages can not be dependent on any project-specific application. Only allowed dependencies are allowed to other laravel packages.

## Who do I talk to? ##

Jakub Lajmon <jakub@lajmon.name>