<?php

return [

	'customer' => 'Zákazník',

	'reg_id' => 'IČO',
	'vat_id' => 'IČ DPH',
	'issue_date' => 'Vystavenie',
	'tax_date' => 'DUZP',
	'due_date' => 'Splatnosť',

	'item' => 'Položka',
	'amount' => 'Množstvo',
	'price' => 'Cena',
	'vat_rate' => 'DPH',
	'sum' => 'Suma',
	'sum_with_vat' => 'Suma s DPH',
	
	'paid' => 'Uhradené',
	'total' => 'Spolu',
	'to_pay' => 'K úhrade',

	'vs' => 'VS',
	'cs' => 'KS',
	'ss' => 'SS',
	'order_id' => 'Objednávka',

];