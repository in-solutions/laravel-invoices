<?php

return [

	'customer' => 'Customer',

	'reg_id' => 'Reg. ID',
	'vat_id' => 'VAT ID',
	'issue_date' => 'Issued',
	'tax_date' => 'Tax date',
	'due_date' => 'Due date',

	'item' => 'Item',
	'amount' => 'Amount',
	'price' => 'Price',
	'vat_rate' => 'VAT rate',
	'sum' => 'Sum',
	'sum_with_vat' => 'Sum with VAT',

	'paid' => 'Paid',
	'total' => 'Total',
	'to_pay' => 'To pay',

	'vs' => 'VS',
	'cs' => 'CS',
	'ss' => 'SS',
	'order_id' => 'Order',

];
