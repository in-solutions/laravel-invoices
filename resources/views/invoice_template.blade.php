<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Invoice</title>

    <style>
    .right {
        text-align: right;
    }

    .left {
        text-align: left !important;
    }

    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 15px;
        line-height: 20px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }

    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }

    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }

    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }

    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }

    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }

    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
        text-align: right;
    }

    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }

        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }

    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr style="font-size: 0.9em">
                            <td class="title" width="30%">
                                <img src="https://www.venuspuzzle.com/html/logo.png">
                            </td>

                            <td>
                                <strong>{{ $invoice->type->title }}</strong><br>
                                {{ __('invoices::invoice.issue_date') }}:<br>
                                {{ __('invoices::invoice.tax_date') }}:<br>
                                {{ __('invoices::invoice.due_date') }}:<br>
                            </td>
                            <td>
                                {{ $invoice->number ?: $invoice->id }}<br>
                                {{ date('Y-m-d', strtotime($invoice->date_issue)) }}<br>
                                {{ date('Y-m-d', strtotime($invoice->date_tax)) }}<br>
                                {{ date('Y-m-d', strtotime($invoice->date_due)) }}<br>
                            </td>
                            <td>
                                {{ __('invoices::invoice.vs') }}:<br>
                                {{ __('invoices::invoice.cs') }}:<br>
                                {{ __('invoices::invoice.ss') }}:<br>
                                {{ __('invoices::invoice.order_id') }}:<br>
                            </td>
                            <td>
                                {{ $invoice->vs ?: '-' }}<br>
                                {{ $invoice->cs ?: '-' }}<br>
                                {{ $invoice->ss ?: '-' }}<br>
                                {{ $invoice->order_id ?: '-' }}<br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                            @isset($invoice->queue->company)
                                {{ $invoice->queue->company->name }}<br>
                                {{ $invoice->queue->company->address->street }}<br>
                                {{ $invoice->queue->company->address->city }}, {{ $invoice->queue->company->address->zip }}<br>
                                {{ $invoice->queue->company->address->country->name }}
                                <br><br>
                                @isset($invoice->queue->company->reg_id)
                                    {{ __('invoices::invoice.reg_id') }}: {{ $invoice->queue->company->reg_id }}<br>
                                @endisset
                                @isset($invoice->queue->company->vat_id)
                                    {{ __('invoices::invoice.vat_id') }}: {{ $invoice->queue->company->vat_id }}<br>
                                @endisset
                                <br>
                                <small>{{ $invoice->queue->legal_text }}</small>
                            @endisset
                            </td>

                            <td width="40%" class="">
                                <strong>{{ __('invoices::invoice.customer') }}</strong><br><br>
                                {{ $invoice->customer->display_name }}<br>
                                {{ $invoice->customer->invoiceAddress->street }}<br>
                                {{ $invoice->customer->invoiceAddress->city }}, {{ $invoice->customer->invoiceAddress->zip }}<br>
                                {{ $invoice->customer->invoiceAddress->country->name }}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <!-- tr class="heading">
                <td>Payment Method</td>
                <td>Check #</td>
            </tr>

            <tr class="details">
                <td>Check</td>
                <td>1000</td>
            </tr-->

            <tr><td colspan="2">
            <table style="font-size: 0.9em;">
                <tr>
                    <td colspan=6>{{ $invoice->note }}</td>
                </tr>

                <tr class="item heading">
                    <td class="left">{{ __('invoices::invoice.item') }}</td>
                    <td width="10%">{{ __('invoices::invoice.amount') }}</td>
                    <td width="10%">{{ __('invoices::invoice.price') }}</td>
                    <td width="5%">{{ __('invoices::invoice.vat_rate') }}</td>
                    <td width="15%">{{ __('invoices::invoice.sum') }}</td>
                    <td width="15%">{{ __('invoices::invoice.sum_with_vat') }}</td>
                </tr>

                @foreach ($invoice->items as $invoiceItem)
                <tr class="item">
                    <td class="left">{{ $invoiceItem->text }}</td>
                    <td>{{ number_format($invoiceItem->amount, 2) }}</td>
                    <td>{{ number_format($invoiceItem->price_wo_vat, 2) }}</td>
                    <td>{{ number_format($invoiceItem->vat_rate, 0) }}%</td>
                    <td><strong>{{ number_format($invoiceItem->price_wo_vat * $invoiceItem->amount, 2) }}</strong></td>
                    <td><strong>{{ $invoice->currency->code . '&nbsp;' . number_format($invoiceItem->price_w_vat * $invoiceItem->amount, 2) }}</strong></td>
                </tr>
                @endforeach


                <!-- tr class="total right" style="font-size: 1.1em">
                    <td colspan=4></td>
                    <td>{{ __('invoices::invoice.total') }}:</td>
                    <td>{{ $invoice->currency->code . '&nbsp;' . number_format($invoice->sum_w_vat, 2) }}</td>
                </tr -->

                @isset($invoice->paid_at)
                    <tr class="total right" style="font-size: 1.1em">
                        <td colspan=4></td>
                        <td>{{ __('invoices::invoice.paid') }}:</td>
                        <td>{{ $invoice->currency->code . '&nbsp;' . number_format($invoice->sum_w_vat, 2) }}</td>
                    </tr>
                @endisset

                <tr class="total right" style="font-size: 1.3em">
                    <td colspan=4></td>
                    <td>{{ __('invoices::invoice.to_pay') }}:</td>
                    <td><strong>{{ $invoice->currency->code . '&nbsp;' . number_format(isset($invoice->paid_at) ? 0 : $invoice->sum_w_vat, 2) }}</strong></td>
                </tr>

            </table>
            </td></tr>

        </table>
    </div>
</body>
</html>
