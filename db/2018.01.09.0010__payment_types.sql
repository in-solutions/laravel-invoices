SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `enm_payment_type` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `enm_payment_type_tsl` (
`id` int(10) unsigned NOT NULL,
  `payment_type_id` int(10) unsigned NOT NULL,
  `language_code` varchar(2) CHARACTER SET utf8 NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `enm_payment_type`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `enm_payment_type_tsl`
 ADD PRIMARY KEY (`id`), ADD KEY `payment_type_id` (`payment_type_id`), ADD KEY `language_code` (`language_code`);


ALTER TABLE `enm_payment_type_tsl`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE `enm_payment_type_tsl`
ADD CONSTRAINT `enm_payment_type_tsl_ibfk_1` FOREIGN KEY (`payment_type_id`) REFERENCES `enm_payment_type` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `enm_payment_type_tsl_ibfk_2` FOREIGN KEY (`language_code`) REFERENCES `enm_language` (`iso_code`) ON UPDATE CASCADE;


ALTER TABLE `t_invoice` ADD `payment_type_id` INT UNSIGNED NULL DEFAULT NULL AFTER `sum_w_vat`, ADD INDEX (`payment_type_id`) ;

ALTER TABLE `t_invoice` ADD FOREIGN KEY (`payment_type_id`) REFERENCES `enm_payment_type`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;



