SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `enm_invoice_type` (
`id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `enm_invoice_type_translations` (
`id` int(11) NOT NULL,
  `invoice_type_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `t_invoice` (
`id` bigint(20) NOT NULL,
  `queue_id` int(11) NOT NULL,
  `number` varchar(255) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `client_id` bigint(20) unsigned DEFAULT NULL,
  `currency_code` varchar(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date_issue` datetime NOT NULL,
  `date_due` datetime NOT NULL,
  `date_tax` datetime NOT NULL,
  `sum_wo_vat` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `sum_w_vat` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `t_invoice_item` (
`id` int(11) NOT NULL,
  `invoice_id` bigint(20) NOT NULL,
  `text` varchar(255) NOT NULL,
  `amount` decimal(20,4) NOT NULL DEFAULT '1.0000',
  `vat_rate` decimal(10,4) NOT NULL,
  `price_wo_vat` decimal(20,4) NOT NULL,
  `price_w_vat` decimal(20,4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `t_invoice_queue` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `last_number` bigint(20) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


ALTER TABLE `enm_invoice_type`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `enm_invoice_type_translations`
 ADD PRIMARY KEY (`id`), ADD KEY `invoice_type_id` (`invoice_type_id`), ADD KEY `language_code` (`language_code`);

ALTER TABLE `t_invoice`
 ADD PRIMARY KEY (`id`), ADD KEY `queue_id` (`queue_id`), ADD KEY `type_id` (`type_id`), ADD KEY `client_id` (`client_id`), ADD KEY `currency_code` (`currency_code`);

ALTER TABLE `t_invoice_item`
 ADD PRIMARY KEY (`id`), ADD KEY `invoice_id` (`invoice_id`);

ALTER TABLE `t_invoice_queue`
 ADD PRIMARY KEY (`id`);


ALTER TABLE `enm_invoice_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
ALTER TABLE `enm_invoice_type_translations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
ALTER TABLE `t_invoice`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
ALTER TABLE `t_invoice_item`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
ALTER TABLE `t_invoice_queue`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;

ALTER TABLE `enm_invoice_type_translations`
ADD CONSTRAINT `enm_invoice_type_state_translations_ibfk_2` FOREIGN KEY (`language_code`) REFERENCES `enm_language` (`iso_code`),
ADD CONSTRAINT `enm_invoice_type_translations_ibfk_1` FOREIGN KEY (`invoice_type_id`) REFERENCES `enm_invoice_type` (`id`);

ALTER TABLE `t_invoice`
ADD CONSTRAINT `t_invoice_ibfk_1` FOREIGN KEY (`queue_id`) REFERENCES `t_invoice_queue` (`id`),
ADD CONSTRAINT `t_invoice_ibfk_2` FOREIGN KEY (`type_id`) REFERENCES `enm_invoice_type` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `t_invoice_ibfk_3` FOREIGN KEY (`currency_code`) REFERENCES `enm_currency` (`code`) ON UPDATE CASCADE;

ALTER TABLE `t_invoice_item`
ADD CONSTRAINT `t_invoice_item_ibfk_1` FOREIGN KEY (`invoice_id`) REFERENCES `t_invoice` (`id`);
