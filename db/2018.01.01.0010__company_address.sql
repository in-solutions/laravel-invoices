ALTER TABLE `t_company` ADD `address_id` BIGINT UNSIGNED NULL AFTER `vat_id`, ADD INDEX (`address_id`) ;

ALTER TABLE `t_company` ADD FOREIGN KEY (`address_id`) REFERENCES `t_address`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
