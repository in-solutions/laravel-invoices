ALTER TABLE `t_invoice` CHANGE `client_id` `customer_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL;

ALTER TABLE `t_invoice` ADD FOREIGN KEY (`customer_id`) REFERENCES `laravel-sandbox`.`t_customer`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;