SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

INSERT INTO `enm_invoice_type` (`id`, `position`) VALUES
(1, 1),
(2, 2);

INSERT INTO `enm_invoice_type_translations` (`id`, `invoice_type_id`, `language_code`, `title`) VALUES
(3, 1, 'en', 'Proforma'),
(4, 2, 'en', 'Tax document - invoice');
