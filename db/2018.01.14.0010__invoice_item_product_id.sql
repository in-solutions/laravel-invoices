ALTER TABLE `t_invoice_item` ADD `product_id` BIGINT UNSIGNED NULL AFTER `invoice_id`, ADD INDEX (`product_id`) ;

ALTER TABLE `t_invoice_item` ADD FOREIGN KEY (`product_id`) REFERENCES `t_product`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;