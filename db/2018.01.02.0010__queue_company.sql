ALTER TABLE `t_invoice_queue` ADD `company_id` BIGINT UNSIGNED NULL AFTER `id`;

ALTER TABLE `t_invoice_queue` ADD INDEX(`company_id`);

ALTER TABLE `t_invoice_queue` ADD FOREIGN KEY (`company_id`) REFERENCES `t_company`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;