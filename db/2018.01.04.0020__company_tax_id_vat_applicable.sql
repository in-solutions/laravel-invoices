ALTER TABLE `t_company` 
	ADD `tax_id` VARCHAR(64) NOT NULL AFTER `vat_id`, 
	ADD `vat_applicable` BOOLEAN NOT NULL DEFAULT FALSE AFTER `tax_id`;