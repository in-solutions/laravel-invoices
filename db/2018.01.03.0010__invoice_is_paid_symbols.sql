ALTER TABLE `t_invoice` ADD `paid_at` TIMESTAMP NULL DEFAULT NULL AFTER `number`;

ALTER TABLE `t_invoice` 
	ADD `vs` VARCHAR(255) NULL AFTER `date_tax`, 
	ADD `ss` VARCHAR(255) NULL AFTER `vs`, 
	ADD `cs` VARCHAR(255) NULL AFTER `ss`, 
	ADD `order_id` VARCHAR(255) NULL AFTER `cs`;