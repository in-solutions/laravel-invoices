SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `t_invoice_queue_tsl` (
`id` bigint(20) unsigned NOT NULL,
  `invoice_queue_id` int(11) NOT NULL,
  `language_code` varchar(2) CHARACTER SET utf8 NOT NULL,
  `legal_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `t_invoice_queue_tsl`
 ADD PRIMARY KEY (`id`), ADD KEY `invoice_queue_id` (`invoice_queue_id`), ADD KEY `language_code` (`language_code`);


ALTER TABLE `t_invoice_queue_tsl`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE `t_invoice_queue_tsl`
ADD CONSTRAINT `t_invoice_queue_tsl_ibfk_1` FOREIGN KEY (`invoice_queue_id`) REFERENCES `t_invoice_queue` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `t_invoice_queue_tsl_ibfk_2` FOREIGN KEY (`language_code`) REFERENCES `enm_language` (`iso_code`) ON UPDATE CASCADE;
