ALTER TABLE `t_invoice` ADD `vat_applicable` BOOLEAN NOT NULL DEFAULT FALSE AFTER `order_id`;

CREATE TABLE IF NOT EXISTS `t_invoice_tsl` (
`id` bigint(20) unsigned NOT NULL,
  `invoice_id` bigint(20) NOT NULL,
  `language_code` varchar(2) CHARACTER SET utf8 NOT NULL,
  `note` varchar(500) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `t_invoice_tsl`
 ADD PRIMARY KEY (`id`), ADD KEY `invoice_id` (`invoice_id`), ADD KEY `language_code` (`language_code`);


ALTER TABLE `t_invoice_tsl`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE `t_invoice_tsl`
ADD CONSTRAINT `t_invoice_tsl_ibfk_1` FOREIGN KEY (`invoice_id`) REFERENCES `t_invoice` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `t_invoice_tsl_ibfk_2` FOREIGN KEY (`language_code`) REFERENCES `enm_language` (`iso_code`) ON UPDATE CASCADE;
